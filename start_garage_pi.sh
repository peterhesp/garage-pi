#! /bin/bash
echo " Start Garage Home Auto server\n";
source /home/pi/HomeAuto/production/venv/bin/activate
flask run -h 0.0.0.0 -p5000 &
RETVAL=$?
PID=$!
echo $PID > ./ha.pid
echo $PID
exit
