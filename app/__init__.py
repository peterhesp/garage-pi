from flask import Flask
from config import Config

from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

import os
import logging
#from .app import app

db = SQLAlchemy()
print('Init application')

def init_logging(log_name):
    print
    from logging.handlers import RotatingFileHandler

    LOG_FORMAT = "%(asctime)s, [%(levelname)s], %(message)s"
    log_dir = os.getcwd()+'/log'
    log_file = log_dir + '/' + log_name + '.log'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    global log
    log = logging.getLogger(log_name)
    handler = RotatingFileHandler(log_file, maxBytes=5000000, backupCount=3)
    formatter = logging.Formatter(LOG_FORMAT)
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.DEBUG)
    return log


def create_app():
    print ('Create App')
    app = Flask(__name__)

    app.config.from_object(Config)
    db.init_app(app)

    log = init_logging('garage')
    
    login = LoginManager()
    login.login_view = 'admin.login'
    login.init_app(app)

    from .models import User

    @login.user_loader
    def load_user(id):
        return User.query.get(int(id))

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint)

    from  .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    log = init_logging('garage')
    return app


app = create_app()

#from . import errors
