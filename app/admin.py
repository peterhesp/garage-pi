from flask import render_template, request, jsonify, Flask, redirect, url_for, flash, Blueprint
from flask_login import login_required, logout_user, current_user, login_user

from .models import User
from . import db

import sys,os
import time
from . import log
from app.forms import LoginForm, RegistrationForm, AddUserForm

#app.config['SECRET_KEY'] = 'secret!'
admin = Blueprint('admin', __name__)


@admin.route('/login', methods=['GET', 'POST'])
@admin.route('/logon', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
         return redirect(url_for('main.garage'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first() 
        if user is None:
            flash('Invalid username or password')
            return redirect(url_for('admin.login'))
        if user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('admin.login'))
        
        login_user(user, remember=form.remember_me.data)
        log.info(current_user.username + " logged IN. "+request.remote_addr+", "+request.headers.get('User-Agent'))
        return redirect(url_for('main.garage'))
    return render_template('login.html', title='Sign In', form=form)

@admin.route('/logout')
@admin.route('/logoff')
@login_required
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('admin.login'))
    log.info(current_user.username + " logged out. "+request.remote_addr+", "+request.headers.get('User-Agent'))
    logout_user()
    return redirect(url_for('admin.login'))

@admin.route('/register', methods=['GET', 'POST'])
def register():
    if not current_user.is_authenticated:
        return redirect(url_for('admin.logon'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, '+user.username+ 'you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@admin.route('/adduser', methods=['GET', 'POST'])
@admin.route('/user/add', methods=['GET', 'POST'])
def add_user():
    if not current_user.is_authenticated:
        return redirect(url_for('admin.login'))

    if current_user.admin != True:
        flash("You must be an administrator to be able to add users.")
        return redirect(url_for('main.garage'))

    form=AddUserForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, is_active=1)
        user.set_password(form.password.data)
        
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('admin.list_users'))
    else:
        print(form.errors)
    return render_template('add_user.html', title='Add User', form=form)


@admin.route('/user/delete/<userid>')
@login_required
def delete_user(userid):
    if current_user.admin != True:
        flash("You must be an administrator to be able to delete users.")
        return redirect(url_for('main.garage'))
    user=User.query.get(userid)
    db.session.delete(user)
    db.session.commit()
    users=User.query.all()

    return redirect(url_for('admin.list_users'))


def change_password(userid):
    user=User.query.get(userid)
    User.query.get(1)
    db.session.commit()


@admin.route('/listUsers')
@login_required
def list_users():
    if current_user.admin != True:
        flash("You must be an administrator to access the List Users page.")
        return redirect(url_for('main.garage'))
    users=User.query.all()
    return render_template('list_users.html', title='List Users', users=users)


@admin.route('/logs', methods=['GET'])
def view_log():
    log =[]
    if not os.path.exists('log/garage.log'):
        os.mknod('log/garage.log')
    with open("log/garage.log") as log_info:
        data = log_info.readline()
        while data: 
            data=data.replace(',', '.', 1)
            data = data.split(',')
            data[-1] = data[-1].strip()
            log.append(data)
            data = log_info.readline()
    return render_template('log.html', log=log)


#if __name__ == '__main__':
#if __name__ == 'app.main':
#    print ('run the Socket')
#    socketio.run(app, debug=True)
