from . import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
#from app import login


class User(UserMixin, db.Model):
#class User( db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    is_active = db.Column(db.Boolean, unique=False, default=True)
    admin = db.Column(db.Boolean, unique=False, default=False)

    def __repr__(self):
        return '<User {}>'.format(self.username)
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


#@login.user_loader
#def load_user(id):
#    return User.query.get(int(id))


