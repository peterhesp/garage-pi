from flask import render_template, request, jsonify, Flask, redirect, url_for, flash
from flask_socketio import SocketIO, emit
from flask_cors import CORS
from flask_login import login_required, logout_user, current_user, login_user
from flask_sse import sse

from flask import Blueprint
from . import db
from . import log
from . import Config

main  = Blueprint('main', __name__)

import sys,os
import time
from gpiozero import Button, LED
from gpiozero import *
from subprocess import check_call
from signal import pause
from .forms import LoginForm, RegistrationForm, AddUserForm

door_1_toggle = LED(5, True, True)
door_2_toggle = LED(6, True, True)

app = Flask(__name__)
app.register_blueprint(sse,url_prefix='/door_state')
app.config["REDIS_URL"]= "redis://localhost"

@main.route("/door_state")
def door_switch_callback(button_number):

    with app.app_context():
       print(button_number.pin.number) 
       if button_number == door_1_opened:
            print('Door One is Open')
            sse.publish({"Door1": 'OPEN'}, type="door_state")
       if button_number == door_1_closed:
            print('Door One is Closed')
            sse.publish({"Door1": 'CLOSED'}, type="door_state")
       if button_number == door_2_opened:
           print('Door Two is Open')
           sse.publish({"Door2": 'OPEN'}, type="door_state")
       if button_number == door_2_closed:
          print('Door Two is Closed')
          sse.publish({"Door2": 'CLOSED'}, type="door_state")
    return

door_1_closed = Button(20)
door_1_closed.when_pressed = door_switch_callback
door_1_opened = Button(21)
door_1_opened.when_pressed = door_switch_callback

door_2_closed = Button(23)
door_2_closed.when_pressed = door_switch_callback
door_2_opened = Button(24)
door_2_opened.when_pressed = door_switch_callback


@main.route('/', methods=['GET', 'POST'])
@main.route('/index', methods=['GET', 'POST'])
@main.route('/garage', methods=['GET', 'POST'])
@login_required
def garage():
    if request.method == 'POST':
        door_no = int(request.form['door_no'])
        # start moving door to it requested state.
        msg=''
        if door_no == 1:
            try:
               door_1_toggle.off()
               time.sleep(0.5)
               door_1_toggle.on()
            except:
                flash("Operation failed")
        elif door_no == 2:
            try:
               door_2_toggle.off()
               time.sleep(0.5)
               door_2_toggle.on()
            except:
                flash("Operation failed")
        log.info(current_user.username + " accessed door " + str(door_no) + ". "+ request.remote_addr + ", "+request.headers.get('User-Agent'))

        return render_template('garage.html', msg=msg, method=request.method)
    else:
         return render_template('garage.html' )


@main.errorhandler(404) 
def not_found(e): 
  return render_template("garage.html") 
