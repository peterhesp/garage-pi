
$(document).ready(function(){
  $('#tab-garage-doors').addClass("active");
  socket.on('message', function(data) {
      console.log('data');
      change_door_state(data);       
  });
  
  $('.tab').click(function() {
        console.log("tabheads");
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        index = $(this).index();

        $("#content > div").removeClass("active");
        $("#content > div:nth-child(" + (index + 1) + ")").addClass("active");
    });

  });

var door_source = new EventSource("/door_state");
source.addEventListener('door_state', 
    function change_door_state(data){
       console.log('DOOR STATE:' +data['door_state']);
       console.log(data);
       if( data['door_name'] == 'door_one' ){
       // change door status div
           $('#door_one_status_txt').html(data['door_state']);
       // change door button
           if (data['door_state'] == 'Open'){
              console.log("OPEN");
              $("#door_one").html('Close');
              $("#door_one").removeClass('open moving');
              $("#door_one").addClass('close');
           }
           else if (data['door_state'] == 'Closed'){
              console.log("CLOSE");
              $("#door_one").html('Open');
              $("#door_one").removeClass('close moving');
              $("#door_one").addClass('open');
          }
          else if (data['door_state'] == 'Moving'){
              console.log("MOVING");
             $("#door_one").html('Stop');
             $("#door_one").removeClass('close open')
             $("#door_one").addClass('moving')
         }
      }
      else{
         console.log('Door Two');  
         $('#door_two_status_txt').html(data['door_state']);
         // change door button
         if (data['door_state'] == 'Open'){
            $("#door_two").html('Close');
            $("#door_two").removeClass('open')
            $("#door_two").removeClass('moving')
            $("#door_two").addClass('close')
         }
         else if (data['door_state'] == 'Closed'){
            $("#door_two").html('Open');
            $("#door_two").removeClass('close')
            $("#door_two").removeClass('moving')
            $("#door_two").addClass('open')
        }
        else if(data['door_state'] == 'Moving'){
           $("#door_two").html('Stop');
           $("#door_two").removeClass('close open')
           $("#door_two").addClass('moving')
        }  
    }
}, false);

function sleep(miliseconds) {
   var currentTime = new Date().getTime();

   while (currentTime + miliseconds >= new Date().getTime()) {
   }
}
function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}
